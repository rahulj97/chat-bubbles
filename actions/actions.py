# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List
#
from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet,AllSlotsReset,FollowupAction
from rasa_sdk.executor import CollectingDispatcher
from openings import openingFunction

#
# class ActionHelloWorld(Action):
#
#     def name(self) -> Text:
#         return "action_hello_world"
#
#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
#
#         dispatcher.utter_message(text="Hello World!")
#
#         return []

class ActionGetName(Action):

  def name(self)-> Text:
    return "action_get_name"
  def run(self,dispatcher:CollectingDispatcher,tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
    name = None
    print(tracker.sender_id)
    for rank in tracker.latest_message['intent_ranking']:
          print(rank["confidence"])
          if rank["confidence"] > 0.85:
            if tracker.get_slot("name") == None:
                for rank in tracker.latest_message['intent_ranking']:
                    print(rank["confidence"])
                name = tracker.latest_message['text'].split(' ')[-1]
                if name == "name":
                    name = tracker.latest_message['text'].split(' ')[0]
                dispatcher.utter_message(text="Hi! "+name+", How can I help you?")
                return [SlotSet("name",name)]
            else:
                for rank in tracker.latest_message['intent_ranking']:
                    print(rank["confidence"])
                name = tracker.latest_message['text'].split(' ')[-1]
                if name == "name":
                    name = tracker.latest_message['text'].split(' ')[0]
                dispatcher.utter_message(text="Hi! "+name+", How can I help you?")
                return [SlotSet("name",name)]
          else:
            dispatcher.utter_message(text="I am not aware of that")
            return []

class ActionName(Action):

  def name(self)-> Text:
    return "action_name"

  def run(self,dispatcher:CollectingDispatcher,tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
    print(tracker.get_slot("name"))
    print("Entered")
    for rank in tracker.latest_message['intent_ranking']:
          print(rank["confidence"])
          if rank["confidence"] > 0.90:
              if tracker.get_slot("name") == None:
                dispatcher.utter_message(text="I didnt quite catch your name!")
              else:
                dispatcher.utter_message(text="Your name is "+tracker.get_slot("name"))
              return []
          else:
              dispatcher.utter_message(text="I am not aware of that")
              return []

class ActionCompany(Action):

  def name(self)-> Text:
      return "action_company"

  def run(self,dispatcher:CollectingDispatcher,tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
      for rank in tracker.latest_message['intent_ranking']:
          if rank["confidence"] > 0.90:
              if tracker.get_slot("company") == None:
                entities = tracker.latest_message['entities']
                company=tracker.get_slot("company")

                dispatcher.utter_message(text=" "+company+" is a global product development and consulting company offering custom software solutions to start-ups, small to mid-sized businesses, and large enterprises.")
                return []
              else:
                dispatcher.utter_message(text=" "+tracker.get_slot("company")+" is a global product development and consulting company offering custom software solutions to start-ups, small to mid-sized businesses, and large enterprises.")
                return []
          else:
                dispatcher.utter_message(text="I am not aware of that")
                return []


class ActionCompany(Action):

  def name(self)-> Text:
      return "action_location"

  def run(self,dispatcher:CollectingDispatcher,tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
    for rank in tracker.latest_message['intent_ranking']:
        if rank["confidence"] > 0.90:
            if tracker.get_slot("company") == None:
              entities = tracker.latest_message['entities']
              company=tracker.get_slot("company")

              dispatcher.utter_message(text=" "+company+" is located in Technopark Trivandrum, InfoPark Cochin, Infopark Koratty, UL Cyber Park Calicut, Chennai, Banglore, USA, Europe, Dubai, Japan, Singapore, Australia")
              return []
            else:
                dispatcher.utter_message(text=" "+tracker.get_slot("company")+" is located in Technopark Trivandrum, InfoPark Cochin, Infopark Koratty, UL Cyber Park Calicut, Chennai, Banglore, USA, Europe, Dubai, Japan, Singapore, Australia")
                return []
        else:
              dispatcher.utter_message(text="I am not aware of that")
              return []

class ActionOpenings(Action):

  def name(self)-> Text:
      return "action_openings"

  def run(self,dispatcher:CollectingDispatcher,tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
    for rank in tracker.latest_message['intent_ranking']:
        print(rank["confidence"])
        if rank["confidence"] > 0.90:
            entities = tracker.latest_message['entities']
            openings=tracker.get_slot("openings")

            dispatcher.utter_message(text="Which location are you interested?")
            return []
        else:
            dispatcher.utter_message(text="I am not aware of that")
            return []

class ActionOpeningsExperience(Action):

  def name(self)-> Text:
      return "action_opening_experience"

  def run(self,dispatcher:CollectingDispatcher,tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
    for rank in tracker.latest_message['intent_ranking']:
        print(rank["confidence"])
        if rank["confidence"] > 0.90:
            entities = tracker.latest_message['entities']
            openings = openingFunction(tracker.get_slot("openings"),tracker.get_slot("location"),tracker.get_slot("experience"))
            print(openings)
            dispatcher.utter_message(text=openings)
            return []
        else:
            dispatcher.utter_message(text="I am not aware of that")
            return []


class ActionGoodbye(Action):

  def name(self)-> Text:
    return "action_goodbye"
  def run(self,dispatcher:CollectingDispatcher,tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
      for rank in tracker.latest_message['intent_ranking']:
          if rank['name'] == "goodbye":
            if rank["confidence"] > 0.97:

              if tracker.get_slot("name") == None:
                  if tracker.get_slot("openings") == None:
                      dispatcher.utter_message(text="Goodbye")
                      return [AllSlotsReset()]
                  else:
                    dispatcher.utter_message(text="Did you get openings for "+tracker.get_slot("openings")+"?")
                    return []
              else:
                if tracker.get_slot("openings") == None:
                    dispatcher.utter_message(text="Goodbye, "+tracker.get_slot("name"))
                    return [AllSlotsReset()]
                else:
                  dispatcher.utter_message(text="Did you get openings for "+tracker.get_slot("openings")+"?")
                  return []
              return [AllSlotsReset()]
            else:
                dispatcher.utter_message(text="I am not aware of that")
                return []


class ActionAffirm(Action):

  def name(self)-> Text:
    return "action_affirm"

  def run(self,dispatcher:CollectingDispatcher,tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
      for rank in tracker.latest_message['intent_ranking']:
          if rank['name'] == "affirm":
            if rank["confidence"] > 0.97:
              if tracker.get_slot("name") == None:
                  dispatcher.utter_message(text="Great, See you again!")
                  return [AllSlotsReset()]
              else:
                dispatcher.utter_message(text="Great, See you again "+tracker.get_slot("name"))
                return [AllSlotsReset()]
            else:
                dispatcher.utter_message(text="I am not aware of that")
                return []

class ActionDecline(Action):

  def name(self)-> Text:
    return "action_decline"

  def run(self,dispatcher:CollectingDispatcher,tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
      for rank in tracker.latest_message['intent_ranking']:
          if rank['name'] == "decline":
            if rank["confidence"] > 0.97:
              if tracker.get_slot("openings") == None:
                  dispatcher.utter_message(text="Sorry to hear that, We will keep you posted if any openings come up")
                  return [AllSlotsReset()]
              else:
                dispatcher.utter_message(text="Sorry to hear that, We will keep you posted if any openings for "+tracker.get_slot("openings")+" come up")
                return [AllSlotsReset()]
            else:
                dispatcher.utter_message(text="I am not aware of that")
                return []
