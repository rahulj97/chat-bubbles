import pandas as pd


def openingFunction(opening,location,experience):
    open = pd.read_csv("Openings.csv")
    print(opening,location,experience)
    experience = int(experience)
    if (open["Openings"] == opening).any():
        get_openings = open[open["Openings"] == opening]
        if (get_openings["Location"] == location).any():
            get_openings = get_openings[open["Location"] == location]
            if (get_openings["Experience"] >= experience).any():
                return str(len(get_openings[open["Experience"] >= experience]))+" openings present. Please Visit https://www.qburst.com/company/career/openings/"
            else:
                return "No openings for that experience"
        else:
            return "No opening in that location"
    else:
        return "No openings found"
