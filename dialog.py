from transformers import AutoModelForCausalLM, AutoTokenizer,AutoModelForSequenceClassification
import torch
import pandas as pd
import re

tokenizer = AutoTokenizer.from_pretrained("microsoft/DialoGPT-large")
model = AutoModelForCausalLM.from_pretrained("microsoft/DialoGPT-large")

tokenizer_score = AutoTokenizer.from_pretrained("microsoft/DialogRPT-updown")
model_score = AutoModelForSequenceClassification.from_pretrained("microsoft/DialogRPT-updown")

def dialog(question):
  s1 = question
  new_user_input_ids = tokenizer.encode(question + tokenizer.eos_token, return_tensors='pt')
  chat_history_ids = model.generate(new_user_input_ids, max_length=1000, pad_token_id=tokenizer.eos_token_id)
  output = tokenizer.decode(chat_history_ids[:, new_user_input_ids.shape[-1]:][0], skip_special_tokens=True)
  print(output,score(question,output).squeeze())
  return output,score(question,output).squeeze()

def answer(question):

  dialogs, score_dialog = dialog(question)
  try:
    if score_dialog > 0.22:
      return "Sorry, I don't have the response for your enquiry. Please feel free to contact: contact@qburst.com"
    else:
      print(dialogs)
      return dialogs
  except:
    return "I did not receive a response"
